---
domain: rfc.tango-controls.org
shortname: 8/Server
name: Device Server Model
status: stable
editor: Lorenzo Pivetta (lorenzo.pivetta@elettra.eu)
---

# 8/Device Server Model

This document provides the specification of the Device Server.

See also: [2/Device](../2/Device.md)

## Preamble

Copyright (c) 2019 Tango Controls Community.

This Specification is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This Specification is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.

This Specification is a [free and open standard](https://web.archive.org/web/20161002091934/http://www.digistan.org/open-standard:definition) and is governed by the Digital Standards Organization's [Consensus-Oriented Specification System](https://web.archive.org/web/20161002092144/http://www.digistan.org/spec:1/coss).

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC-2119](https://tools.ietf.org/html/rfc2119).


## Tango Device Server specification

 This specification formally documents the Tango Device Server model. 
 The runtime implementation of the Device Server model in a Tango compliant implementation MUST follow this specification.

### Goals

 In a Tango Control System a Device Server is in charge of managing the life cycle and the communication protocol of one or more Tango Devices.
 The Device Server can also offer different services to the clients as well as for the Device.

Additionally, it aims to:

* Provide a blackbox that registers the operations executed on every Device.

* Be usable as a Device via the Administration Device referred to as DServer.

### Use Cases

The main use cases for DServer are:

* To manage several Devices in the same process. 

* To share the same channel of communication.

* To provide the connection protocols for accessing the managed Device.

* To provide different services like logging.

* To serialise the operations per Device.

* To export or unexport Devices to the central registry.



## Specification

The Device Server manages the communication with one or several Devices.  Additionally it enforces the compatibility of behaviour with the Tango Control System and offers different meta services.

The Device Server SHALL implement the following specifications to control and monitor the Device:

* A Device Server represents the process which manages one or several Devices.

* A Device Server SHALL manage at least one Device. 

* A Device Server MAY manage Devices belonging to different Classes.

* A Device MUST NOT be managed by more than one Device Server.

* A Device Server instance reference SHALL be unique in a Tango Control system.

* The communication to the Device Server SHALL be done through a Tango Device DServer also-called Administration Device or Admin Device.


The specification defines the following concepts which are used by the Administration Device:

* A Device Server is a process which creates and serves devices.

* Several instances of the same Device Server MAY exist in a Tango Control System.

* Instance is an unique identifier which together with the Device Server name identifies the unique process in the Tango Control System.

* A Device Server Instance CAN only have one DServer Device instance.


The name of the DServer / Administration Device SHALL follow this convention:
```ABNF
administration-device-name = dserver "/" server "/" instance
dserver = "dserver" ; dserver is a reserved domain following the device-name convention.
server = family
instance = member
```

* The family part of the Administration Device Name SHALL be the Server name.

* The member part of the Administration Device Name SHALL be the Instance.



### DServer interface


| Property Name | description |
|---|---|
| polling_threads_pool_size |  See [10/RequestReply Cache](../10/RequestReply.md#cache) |
| polling_threads_pool_conf |  See [10/RequestReply Cache](../10/RequestReply.md#cache) |
| polling_before_9 |  See [10/RequestReply Cache](../10/RequestReply.md#cache) |

| Command Name | Argument input | Argument Output | description |
|---|---|---|---|
| AddLoggingTarget | DevVarStringArray | DevVoid | Define the level of Loggin ([14/Logging](../14/Logging.md) |
| AddObjPolling | DevVarLongStringArray | DevVoid | Polling related command ([10/RequestReply](../10/RequestReply.md)) |
| DevLockStatus | DevString | DevVarLongStringArray | Request Replay command ([10/RequestReply](../10/RequestReply.md)) |
| DevPollStatus | DevString | DevVarStringArray | Polling related command ([10/RequestReply](../10/RequestReply.md)) |
| DevRestart | DevString | DevVoid |
| EventConfirmSubscription | DevVarStringArray | DevVoid |
| EventSubscriptionChange | DevVarStringArray | DevLong |
| GetLoggingLevel | DevVarStringArray | DevVarLongStringArray |
| GetLoggingTarget | DevString | DevVarStringArray |
| Init | DevVoid | DevVoid |
| Kill | DevVoid | DevVoid |
| LockDevice | DevVarLongStringArray | DevVoid |
| PolledDevice | DevVoid | DevVarStringArray |
| QueryClass | DevVoid | DevVarStringArray |
| QueryDevice | DevVoid | DevVarStringArray |
| QuerySubDevice | DevVoid | DevVarStringArray |
| QueryWizardClassProperty | DevString | DevVarStringArray |
| QueryWizardDevProperty | DevString | DevVarStringArray |
| ReLockDevices | DevVarStringArray | DevVoid |
| RemObjPolling | DevVarStringArray | DevVoid |
| RemoveLoggingTarget | DevVarStringArray | DevVoid |
| RestartServer | DevVoid | DevVoid |
| SetLoggingLevel | DevVarLongStringArray | DevVoid |
| StartLogging | DevVoid | DevVoid |
| StartPolling | DevVoid | DevVoid |
| State | DevVoid | DevState |
| Status | DevVoid | DevString |
| StopLogging | DevVoid | DevVoid |
| StopPolling | DevVoid | DevVoid |
| UnLockDevice | DevVarLongStringArray | DevLong |
| UpdObjPollingPeriod | DevVarLongStringArray | DevVoid |
| ZmqEventSubscriptionChange | DevVarStringArray | DevVarLongStringArray |

### The Device Export sequence

The Device Export sequence transitions a Device from the Not Exported phase to the Exported phase. 
It is intended to prepare a Device for operation and accessibility in the Tango Control System:

* The Device Server MUST take in charge of the Device Export sequence.

* The Device Export sequence SHOULD bring the Device at the same State than after the Init Command.

* The Device Export sequence SHOULD register the Device to the Tango Control system by executing the Device Sign Up of the Database([6/Database](../6/Database.md)), in order to be accessible locally and via network.


### The Device Unexport sequence

The Device Unexport sequence transitions a Device from the Exported phase to the Not Exported phase.
It is intended to prevent a Device from operation and accessibility in the Tango Control System.

* The Device Server MUST take in charge of the Device Unexport sequence.

* The Device Unexport sequence SHALL gracefully stop the Device operation, even if there are requests in progress.

* The Device Unexport sequence SHALL gracefully stop the communication with any Tango Client .

* The Device Unexport sequence SHOULD unregister from the Tango Control system by executing the Device unexport of the Database([6/Database](../6/Database.md)), in order to notify that all managed Device are no longer accessible.
