---
domain: rfc.tango-controls.org
shortname: 1/TANGO
name: Tango Control System
status: stable
editor: Lorenzo Pivetta (lorenzo.pivetta@elettra.eu)
---

# 1/Tango

This document describes the philosophy of Tango, a control system framework.
This RFC specifies a control system implementing the philosophy of Tango.

See also: All Tango RFC's

## Preamble

Copyright (c) 2019 Tango Controls Community.

This Specification is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This Specification is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.

This Specification is a [free and open standard](https://web.archive.org/web/20161002091934/http://www.digistan.org/open-standard:definition) and is governed by the Digital Standards Organization's [Consensus-Oriented Specification System](https://web.archive.org/web/20161002092144/http://www.digistan.org/spec:1/coss).

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC-2119](https://tools.ietf.org/html/rfc2119).

## Tango Specification

The specification contained in the RFCs describes the preferred behaviour of the latest stable version (V9) of Tango. The specification is considered to be the desired behaviour and any implementation (in whichever language) differing from the specification is considered non-compliant.
Anyone spotting such differences  is encouraged to file an issue for the concerned implementation.

### Goals

The *Tango toolkit* was initially developed for the needs of large research facilities, although the concept (philosophy) offers a framework for any control systems in general. It is based on the idea that devices represented as distributed objects are ideally suited to design and implement control systems which are flexible and capable of handling complexity. In this sense it aims to provide:

* an **Open Source Software** toolkit for building distributed object-oriented control systems. It should be freely available for all applications including industry. Tango Controls is sufficiently complete to control any installation small or large. 
* a **Programmable way** which allows hardware to be interfaced and modelled in a device object model with a suited programming language. 
* the tools for **configuring, managing, monitoring and archiving** large numbers of control points implemented as device attributes. 
* the **naming, browsing and hierarchies** of devices and groups of devices.

It offers additionally the following qualities:

* **Scalable** for *building large and small control systems*. There should be no limit on the number of hardware and software devices the control system can control.
* **Usable** to replace proprietary industrial control systems.
* **Compatible** with a wide range of *communication protocols* and *industrial hardware standards*.

### Concepts

The control system used in large facilities has to monitor and process large to huge amounts of data in real time and archive them for analysis and post processing.

**Tango** aims to interface almost any kind of hardware or software system, for which a well defined API exists, in order to represent these systems as Tango Devices within a control system (see [2/Device](../2/Device.md)).

**Tango Devices** are software components which have *[Commands](../3/Command.md)* (methods), *[Attributes](../4/Attribute.md)* (data), *[Properties](../5/Property.md)* (configuration) and *[Pipes](../7/Pipe.md)* (data streaming) and are accessible on the network or locally.

A **Tango control system** usually consists of a collection of Tango Devices, a Tango Database and a set of applications for configuring and controlling the Devices via well defined Application Programmers Interfaces (APIs).

The *Tango Database* provides a directory service to look up a Device's location in the control system for a client to directly establish a network connection with it (see [6/Database](../6/Database.md)). The *Tango Database* also store the configuration parameters for Devices and the relevant part of the control system. 


### Requirements

The critical requirements from the original design are [1] :

1. That the *Devices* are network objects with the minimum of functionality necessary for a distributed control object.
2. Use *CORBA* communication protocol while hiding details [2].
3. The support of the  *synchronous, asynchronous and event driven* communication paradigms.
4. The support of *Device hierarchies* by inheritance and composition.
5. The support modern programming patterns like *multi-threading*.
6. Using a *naming database* which stores configuration information.
7. Using a generic, but finite, set of *Data Types* [3].
8. To be able to *monitors* Devices by polling.
9. To be able to cache Attribute.
10. The support for *scripting*
11. The support for *multiple platforms*

References:

[1] "TANGO - AN OBJECT ORIENTED CONTROL SYSTEM BASED ON CORBA" by Chaize et. al., ICALEPCS 1999
https://www.elettra.trieste.it/ICALEPCS99/proceedings/papers/wa2i01.pdf

[2] current implementations of Tango use CORBA/ZMQ as the communication protocol however the RFC's do not impose this. Other communication protocols can be used to implement the Tango control system concepts and features.

[3] a rich set of over 20+ generic types which can be composed in [Pipes](../7/Pipe.md) are supported. Adding of new types is discouraged.
