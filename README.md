# Tango Request For Comments (RFC)

This repository is the home of all Tango Open Specification.

It is rendered on the readthedocs.io: [![Documentation Status](https://readthedocs.org/projects/rfc/badge/?version=latest)](http://tango-controls.readthedocs.io/projects/rfc/en/latest)

## Mission

The goal of this RFC project is to provide a formal specification of the current (V9) Tango Controls system. This specification shall include:

1. Concepts,
2. Terminology,
3. Protocol behaviour,
4. Conventions,

each on a sufficient level for future evolution of Tango Controls and/or implementation in other languages. In that respect, concepts are more important than implementation details.


## Contribution

The process to add or change an RFC is the following:

- An RFC is created and modified by pull requests according to the Collective Code Construction Contract [(C4)](https://github.com/unprotocols/rfc/blob/master/1/README.md).
- The RFC life-cycle SHOULD follow the life-cycle defined in the Consensus-Oriented Specification System [(COSS)](https://github.com/unprotocols/rfc/blob/master/2/README.md).
- The specification SHOULD use Augmented BNF for Syntax Specifications: ABNF [(RFC5234)](https://www.rfc-editor.org/rfc/rfc5234)

Read more [here](https://gitlab.com/tango-controls/rfc/-/wikis/home).

## RFCs

The table below summarises all available or expected specifications.

| Short Name                                                                | Title                                                     | Status      | Editor               |
|---------------------------------------------------------------------------|-----------------------------------------------------------|-------------|----------------------|
| [1/Tango](1/Tango.md)                                                     | The Tango control system                                  | Stable       | Lorenzo Pivetta      |
| [2/Device](2/Device.md)                                                   | The device object model                                   | Stable       | Vincent Hardion      |
| [3/Command](3/Command.md)                                                 | The command model                                         | Stable       | Sergi Blanchi-Torné  |
| [4/Attribute](4/Attribute.md)                                             | The attribute model                                       | Stable       | Sergi Blanchi-Torné  |
| [5/Property](5/Property.md)                                               | The property model                                        | Stable       | Gwenaelle Abeillé    |
| [6/Database](6/Database.md)                                               | The database system                                       | Stable       | Gwenaelle Abeillé    |
| [7/Pipe](7/Pipe.md)                                                       | The pipe model                                            | Stable       | Reynald Bourtembourg |
| [8/Server](8/Server.md)                                                   | The server model                                          | Stable       | Lorenzo Pivetta      |
| [9/DataTypes](9/DataTypes.md)                                             | Data types                                                | Stable       | Gwenaelle Abeillé    |
| [10/RequestReply](10/RequestReply.md)                                     | The Request-Reply protocol                                | Stable       | Reynald Bourtembourg |
| 11/RequestReplyCORBA                                                      | The Request-Reply protocol - CORBA implementation         | Postponed   |                      |
| [12/PubSub](12/PubSub.md)                                                 | The Publisher-Subscriber protocol                         | Draft       | Vincent Hardion      |
| [13/PubSubZMQ](https://gitlab.com/tango-controls/rfc/-/merge_requests/39) | The Publisher-Subscriber protocol - ZeroMQ implementation | Postponed |                      |
| [14/Logging](14/Logging.md)                                               | Logging service                                           | Stable       | Sergi Blanchi-Torné  |
| [15/DynamicAttrCmd](15/DynamicAttrCmd.md)                                 | The dynamic attribute and command                         | Stable       | Reynald Bourtembourg |
| [16/TangoResourceLocator](16/TangoResourceLocator.md)                     | Tango Resource Locator                                    | Stable       | Thomas Braun         |
| 17/MemorisedAttr                                                          | Memorised attribute service                               | Postponed   |                      |
| 18/AccessCtrl                                                             | Authorisation system                                      | Postponed   |                      |
