---
domain: rfc.tango-controls.org
shortname: 16/TangoResourceLocator
name: Tango Resource Locator
status: stable
editor: Thomas Braun (thomas.braun@byte-physics.de)
---

# 16/Tango Resource Locator (TRL)

This document describes the Tango Resource Locator.

See also: [1/Tango](../1/Tango.md), [2/Device](../2/Device.md), [4/Attribute](../4/Attribute.md), [5/Property](../5/Property.md), [6/Database](../6/Database.md).

## Preamble

Copyright (c) 2019 Tango Controls Community.

This Specification is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This Specification is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.

This Specification is a [free and open standard](http://www.digistan.org/open-standard:definition) and is governed by the Digital Standards Organization's [Consensus-Oriented Specification System](http://www.digistan.org/spec:1/COSS).

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119](http://tools.ietf.org/html/rfc2119).


## Tango Resource Locator (TRL) Specification

The distributed objects that make up a Tango control system need to be accessible on a network, as follows:

* Each distributed object MUST only be defined in a single Tango Database.
* Each distributed object MUST have a unique identifier within the network - this is termed the Tango Resource Locator.
* Each Tango Resource Locator MUST be sufficient for a client on the network to initiate communication with the specified object.
* All Tango Resource Locators MUST be case insensitive.
* Clients MAY use shortened forms of the Tango Resource Locator, but additional context MUST be used to disambiguate the name.
* Such additional context MAY include environment variables and data from configuration files.
* There MAY be additional names that refer to the same distributed object, such a name is termed an Alias.

* The Tango Resource Locator MUST use the following scheme:

```
[protocol://][host:port/]device-name[/attribute][->property][#dbase=xx]
```

Following the convention:

* `protocol`:

    ```ABNF
    protocol = %s"tango"
    ```

    * If omitted, the implementation MUST use `tango://`.

* `host`: MUST be either

    * an IP address (Only IPv4 for the moment) or
    * a resolvable hostname

    as defined in [RFC 1123](https://datatracker.ietf.org/doc/html/rfc1123) and [RFC 952](https://datatracker.ietf.org/doc/html/rfc952).

* `port`:  MUST be an integer in the range of 1 to 65535 as defined in [RFC 793](https://datatracker.ietf.org/doc/html/rfc793#section-3.1). It is
    RECOMMENDED to not use privileged (1 to 1024) ports.

* If `host` and `port` are omitted:

    * The implementation MUST set it based on the client's local context (environment variables, config files, etc.):
        * The local context MAY include the environment variable `TANGO_HOST`, set to a string providing `host:port`.
        * If the local context does not provide sufficient detail then access MUST fail.

* `device-name`: as specified in [2/Device](../2/Device.md).

* `attribute` and `property`:

    * `attribute`: as specified in [4/Attribute](../4/Attribute.md).
    * `property`: as specified in [5/Property](../5/Property.md).
    * If `attribute` is omitted, but `property` is included it MUST name a Device Property.
    * If both `attribute` and `property` are included they MUST name an Attribute Property.

* `dbase` and `xx`:

    ```ABNF
    dbase = "dbase"
    xx = "yes" / "no"
    ```

    * If `#dbase=xx` is omitted, the implementationr MUST use `#dbase=yes`.

    * If `#dbase=no`, then the implementation MUST not attempt to access a Tango Database when establishing the connection with a Device/Attribute/Property.

**Note**: The Tango Resource Locator looks similar to a uniform resource locator, as per [RFC 3986](https://datatracker.ietf.org/doc/html/rfc3986), but the current implementation has minor deviations from RFC 3986. This specification will not refer to the Tango Resource Locator as a uniform resource locator (URL). Existing Tango documentation sometimes refers to the Tango Resource Locator as the Fully Qualified Domain Name (FQDN). The FQDN is also ambiguous, so it is not used in this specification.

### Case sensitivity

Tango Controls does not distinguish between upper and lower case when interpreting the following names:

Device Server, Device, Device Alias, Command, Attribute, Property

**Note**: When storing any of the above values in the Tango database or declaring them in the code for readability then the case is preserved however all comparisons ignore case and all permutations of the case e.g. 'VOLTAGE'== 'voltage'.

All Property values, Class names and DServer (device server) names are exempt from this and are interpreted as case sensitive.

### Alias

The aliasing mechanism is provided for convenience.  It could provide a shorter or more memorable name for an object in the Tango control system.

* An Alias MAY refer to a Device.
* An Alias MAY refer to a Device Attribute.
* An Alias MUST be unique within a Tango control system (single Tango Database).
* An Alias MUST be stored in the Tango Database.
* The Device Alias specification is in [2/Device](../2/Device.md).
* The Attribute Alias specification is in [4/Attribute](../4/Attribute.md).
* Additional context MUST be used to find the Tango Database, in the same way that a shortened Tango Resource Locator is disambiguated.

### Examples

```
tango://db.example.com:10000/lab/powersupply/01
```

This refers to a Device named `lab/powersupply/01`. The Device Server which is running the Device is running on an unknown host. Therefore a lookup will be performed by the Tango Database which runs on host `db.example.com` at port 10000 to make a client to Device connection possible.

```
LAB/POWERSUPPLY/01
```

This TRL refers to a Device named `lab/powersupply/01`. It must be defined in the Tango Database. The host and port for the Tango Database needs to be discovered in the local context. An example for a matching full TRL with a Tango Database at `db.example.com:10000` is `tango://db.example.com:10000/lab/powersupply/01#dbase=yes`.

**Note** that the case is ignored.

```
tango://db.example.com:10000/lab/powersupply/01/voltage
```

This TRL refers to the `voltage` Attribute on the `lab/powersupply/01` Device. The Device is defined in the Tango Database, which runs on `db.example.com` at port 10000.

```
tango://db.example.com:10000/lab/powersupply/01/voltage->unit
```

This refers to the  `unit` Property of the `voltage` Attribute on the same Device.

```
tango://db.example.com:10000/lab/powersupply/01->address
```

This refers to the `address` Device Property on the same Device.

```
tango://lab.example.com:14555/lab/powersupply/01#dbase=no
```

This refers to a Device named `lab/powersupply/01`. Regardless if it is defined in a Tango Database or not, the Device will be directly accessed on the host `lab.example.com` at port 14555.

```
tango://lab.example.com:14555/lab/powersupply/01/voltage#dbase=no
```

This refers to the `voltage` Attribute on the same Device.

