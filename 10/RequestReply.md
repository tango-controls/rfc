---
domain: rfc.tango-controls.org
shortname: 10/RequestReply
name: The Request-Reply protocol
status: stable
editor: Reynald Bourtembourg <reynald.bourtembourg@esrf.fr>, Thomas Juerges <thomas.juerges@skao.int>
---

# 10/Request Reply

This document describes The Request-Reply protocol which provides a two way communucation between the Tango
Controls clients and servers.

See also: Y/OtherTemplate

## Preamble

Copyright (c) 2019 Tango Controls Community.

This Specification is free software; you can redistribute it and/or modify it under the terms of the GNU General Public 
License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later 
version. This Specification is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public 
License for more details. You should have received a copy of the GNU General Public License along with this program; 
if not, see <http://www.gnu.org/licenses>.

This Specification is a [free and open standard](https://web.archive.org/web/20161002091934/http://www.digistan.org/open-standard:definition) and is governed by the Digital Standards Organization's [Consensus-Oriented Specification System](https://web.archive.org/web/20161002092144/http://www.digistan.org/spec:1/coss).

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", 
and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119](http://tools.ietf.org/html/rfc2119).

## The Request-Reply protocol Specification

The Request-Reply protocol defines:
* Types and content of messages sent between Tango Controls objects.
* Communication sequences.
* Rules for establishing and tearing-down network connections.

### Goals

The Request-Reply protocol aims to standardise way of exchanging data between Tango Clients and Device Servers in 
a pattern where a client sends a request and the server reply with an answer. It SHALL allow for reading and writing 
Attributes and Pipes as well as for executing Commands.

Additionally, it aims to:

* Be independent of network transport/binary protocol.

* Define a rules for transparent reconnection.

* Allow for initiation and management of other communication protocols (like Publisher-Subscriber protocol).

### Use Cases

There are few main use cases for The Request-Reply protocol:

* Calling Commands on Devices.
* Reading Attributes' and Pipes' values and meta-data.
* Writing Attributes' and Pipe's data.
* Setting-up Publisher-Subscriber communication.

## Specification

### Client duty

All requests MUST be initiated by the client.

#### Reconnection

The client SHALL manage all connections to the Device Servers.

When sending any request to a Device Server the client:

* SHALL check if the connection to the Device Server is established and active.
* SHALL try to connect to the Device Server if the connection is not yet established or broken.
* SHALL inform the user by throwing a DevFailed exception if the connection cannot be made within a specified time defined by a user (timeout).
* SHALL use this connection to send the request if the connection is established and active.
* if the client gets no response within a certain time defined by a user (timeout) the connection SHALL be set to broken (teardown) and the client SHALL inform the user via throwing a DevFailed exception.
* SHALL respond adequately to any information on the connection state changes provided by the connection itself (e.g. os level exceptions). 

* The Request-Reply protocol SHALL implement Transparent Reconnection mechanism on the client side. Transparent Reconnection is hiding temporary connection issues by re-trying of connection establishment.

* The Client MAY opt-out from using Transparent Reconnection.

### Server duty

* The server MUST process all requests it receives.
* For every received request the server MUST send a reply to the client, in fire and forget fashion. 


### Version compatibility

The current network protocol version is 5. Devices and Clients MUST support at least network protocol version 5. Devices and Clients MAY support different network protocol versions. It is RECOMMENDED that Devices and Clients support network protocol version 3 and 4 too. When connecting to a Device, it is RECOMMENDED that a Client determines the highest possible common network protocol version. When communication with a Device, a Client SHALL use a network protocol version that is not higher than their highest common network protocol version. A Device MUST not use a network protocol version higher or lower than the network protocol version used by a Client when communicating with this Client.


| network protocol Version Client uses | network protocol Version Device | Expected behaviour |
| :---------------------: | :----------------: | ----------------------------------------------------------- |
|          C > D          |         D          | The Client MUST support the complete specification of the Device's network protocol version D. The Client MUST not use any features from an network protocol version higher than D. |
|          C < D          |         D          | The Client MUST support network protocol version C. The Device MUST use network protocol version C to communicate with this Client. |
|          C = D          |         D          | The Client and the Device both fully support each other's network protocol version. |

### Protocol

#### Message
The Request-Reply protocol does not specify a binary structure of the message nor its encoding. The encoding and the binary format SHOULD be defined in an implementation RFC.

The Message implies that a Client has already made a connection to the Device. See [Connection management](#### Connection management)

However, this RFC specifies the information exchange between the Client and the Device. The communication always starts with a Request from a Client. The Device SHALL reply to any Client requests.
The Request can be Synchronous or Asynchronous, see the specification below.

All Messages SHALL contain:

* Request type (see table below).
* Version of the protocol, since version 2.

It is RECOMMENDED for a Message to contain an id of the request.

Table of supported request type:

| Request type | Version | Alternative |
| ------------ | ------- | ----------- |
| DEVICE NAME | 3 |
| DEVICE DESCRIPTION | 3 |
| DEVICE ADM_NAME | 3 |
| DEVICE PING | 3 |
| DEVICE BLACKBOX | 3 |
| DEVICE INFO | 3 |
| COMMAND EXECUTION | 3 |
| COMMANDS LIST | 3 |
| COMMAND INFO | 3 |
| COMMAND HISTORY | 3 |
| ATTRIBUTE READ | 3 | See ATTRIBUTES READ |
| ATTRIBUTE WRITE | 3 | See ATTRIBUTES WRITE |
| ATTRIBUTES READ | 3 |
| ATTRIBUTES WRITE | 3 |
| ATTRIBUTE WRITE READ | 4 | See ATTRIBUTES WRITE READ |
| ATTRIBUTES WRITE READ | 5 |
| ATTRIBUTE SET CONFIG | 3 |
| ATTRIBUTE GET CONFIG | 3 |
| ATTRIBUTE HISTORY | 3 |
| PIPE READ | 5 |
| PIPE WRITE | 5 |
| PIPE WRITE READ | 5 |
| PIPE GET CONFIG | 5 |
| PIPE SET CONFIG | 5 |

##### DEVICE NAME

A Device name request does not contain any additional argument.

A reply for Device name request SHALL contain:
* The Device Name

For reference see also [2/Device](../2/Device.md).

##### DEVICE DESCRIPTION

A Device description request does not contain any additional argument.

A reply for Device description request SHALL contain:
* The Device description

For reference see also [2/Device](../2/Device.md).

##### DEVICE ADM_NAME

A Device Admin Name request does not contain any additional argument.

A reply for Admin Device Name request SHALL contain:
* The name of Admin Device for the Device

For reference see also [2/Device](../2/Device.md).

##### DEVICE PING
A Device Ping request does not contain any additional argument.

A device Ping reply consists of an acknowledgement that the command has been successfully executed.

The client implementation MAY return the elapsed time between the request and the reception of the reply.

##### DEVICE BLACKBOX

A Blackbox call request message SHALL contain:

* The number of requested records. If more records are requested than are available, then only the available records are returned.

A Blackbox call reply message SHALL contain:

* Array of at most nb black box entries as strings. It is RECOMMENDED that each string contains the following information: 
  * time: the time when the operation was executed.
  * operation: the type of operation executed on the Device e.g. 'command_inout or read_attribute.
  * source: source of operation (DEVICE or CACHE).
  * client_id: the host+pid or other identifier of the client.
  * client_type: type of client (e.g. C++, Java or Python client).

##### DEVICE INFO

A DevInfo request SHALL contact the server and return information about the Device and Server.

A reply for DevInfo request SHALL contain the following information:

* Device class name
* Server identifier
* Server host
* Server version number
* Link to the documentation url
* Device type name
* Version info list for Tango and its dependencies

The version info list SHALL contain the current Tango binding name (e.g. cppTango, PyTango or JTango) and string version. If different, underlying Tango library implementation (e.g. cppTango in the case of PyTango) MUST also be present in the list.

The device code version, and dependencies, or binding versions that provide meaningful information about the server environment MAY be added by each implementation layer:

  ["MyDevice=1.2.1", "MyDeviceCommsLib=1.4.3", "PyTango=10.0.0","cppTango=10.0.0", "omniORB=4.3.3", "OS=GNU/Linux"]


##### COMMAND EXECUTION
A Command call request message SHALL contain in addition:

* command name
* argin, the specific structure is distinct to implementation
* request type: CMD_INOUT
* source (cache or actual read), since version 2
* client ID, since version 4


A Command call reply message SHALL contain:

* Type of the returned value.
* Value returned by the command execution (so called argout).

For reference, see also [3/Command](../3/Command.md).

##### COMMANDS LIST

The COMMANDS LIST request queries a Device and its reply informs a client of the Device about all of the Device Commands.

A COMMANDS LIST request does not have any additional parameters.

A COMMANDS LIST reply MUST include a sequence of a Device Command Information.

A Device Command Information MUST contain:
* The Command Name
* The Command Display Level as defined in [3/Command](../3/Command.md)
* An Input Data Type as defined in [9/DataTypes](../9/DataTypes.md)
* The Output Data Type as defined in [9/DataTypes](../9/DataTypes.md)
* The Input Data Type Description
* The Output Data Type Description

For reference, see also [3/Command](../3/Command.md) and [9/DataTypes](../9/DataTypes.md).

##### COMMAND INFO 

The COMMAND INFO request queries a Device and its reply informs a client of the Device about one of the Device's Commands.

A COMMAND INFO request MUST have a one parameter:

* The Command Name

A COMMAND INFO reply MUST include a Device Command Information.

The Device Command Information MUST contain:
* The Command Name
* The Command Display Level as defined in [3/Command](../3/Command.md)
* The Input Parameter Data Type as defined in [9/DataTypes](../9/DataTypes.md)
* The Output Parameter Data Type as defined in [9/DataTypes](../9/DataTypes.md)
* The Input Parameter Data Type Description
* The Output Parameter Data Type Description

For reference, see also [3/Command](../3/Command.md) and [9/DataTypes](../9/DataTypes.md).

##### COMMAND HISTORY

The command history allows to report of the latest Command Execution processed by the Device.

A command history request message SHALL contain:

* The Command Name
* The number of requested entries


Until version 3 a collection of tuples with the following elements

A command history reply message SHALL contain:

* Time
* Command passed/failed state
* Command return value
* List of errors

The size of the returned collection is the minimum of the requested entries, the stored number of entries and configured depth.

Starting from verson 4:

A command history reply message SHALL contain:

* List of timestamps
* Command return value
* Tuple of x/y dimensions
* List of dimension ranges defined as index and length
* List of List of errors
* List of list of ranges into the errors array defined as index and length
* Command return type, see [3/Command](../3/Command.md)

The size of the returned first-level lists is the minimum of the requested entries, the stored number of entries and configured depth.

##### ATTRIBUTES READ

An Attributes read request message SHALL contain:

* List of attributes' names to read,
* Source (cache or actual read), since version 2,
* Client ID, since version 4.

Attributes read reply message SHALL contain:

* Sequence of attributes values. Each value SHALL provide:  
  * one read value of Data Type of Attribute
  * quality
  * data_format
  * data_type
  * time
  * name
  * read dim x
  * read dim y
  * w_dim_x
  * w_dim_y
  For reference, see also [4/Attribute](../4/Attribute.md).

##### ATTRIBUTES WRITE
An Attributes write request message SHALL contain:

  * client ID, since version 4

  A collection of the attributes to write where each of them contains:
    * value
    * data_type (one of Tango::AttributeDataType)
    * data_format (one of Tango::SCALAR/SPECTRUM/IMAGE), since version 4
    * name
    * quality
    * time
    * read dim x (until version 3)
    * read dim y (until version 3)
    * write dim x (since version 4)
    * write dim y (since version 4)

  For reference, see also [4/Attribute](../4/Attribute.md).

In case of success the reply message MUST contain nothing, on error it SHALL contain error information.

##### ATTRIBUTES WRITE READ

An Attributes write request message SHALL contain:

  * client ID, since version 4

  A collection of the attributes to write where each of them contains:

    * value
    * data_type (one of Tango::AttributeDataType)
    * data_format (one of Tango::SCALAR/SPECTRUM/IMAGE), since version 4
    * name
    * quality
    * time
    * write dim x (since version 4)
    * write dim y (since version 4)

An Attributes write read reply message SHALL provide the same information as the ATTRIBUTES READ reply.

For reference, see also [4/Attribute](../4/Attribute.md).

##### ATTRIBUTES GET CONFIG
An Attributes get config request message SHALL contain:
* a list of attribute names
 * Exception: The name "All attributes_3" can be used to get all attributes of the Device since version 3
 * Exception: The name "All attributes" can be used to get all attributes of the Device until version 2

An Attribute get config reply message SHALL contain the requested list of Attribute Properties (as defined in [4/Attributes](../4/Attribute.md)).

Each Attribute Properties MUST include:
* The Attribute Name 
* The writable Type
* The memorized mode
* The memorized initialisation mode
* The Data Format (as specified in [9/DataTypes](../9/DataTypes.md)) 
* The Data Type 
* The maximum dimension x 
* The maximum dimension y
* The description 
* The label 
* The unit 
* The standard unit 
* The display unit 
* The format of the displayed value 
* The minimum limit 
* The maximum limit 
* The minimum alarm level
* The maximum alarm level
* The minimum warning level
* The maximum warning level
* The delta t 
* The delta val
* The Attribute Name to write when the writable mode is "READ_WITH_WRITE"
* The Root Attribute Name when the Attribute is of ForwardedAttribute type
* The Display level
* The enumeration labels for the Enum Type
* The Event relative change
* The Event absolute change
* The Archive relative change
* The Archive absolute change
* The Event period
* The Archive period
* Any other configuration for future extensions
* Any other system configuration for future extensions

##### ATTRIBUTES SET CONFIG

An Attributes set config request message SHALL contain:
* A list of Attribute Properties (as defined in [4/Attributes](../4/Attribute.md))

Each Attribute Properties MUST include:
* The Attribute Name 
* The writable Type
* The memorized mode
* The memorized initialisation mode
* The Data Format (as specified in [9/DataTypes](../9/DataTypes.md)) 
* The Data Type 
* The maximum dimension x 
* The maximum dimension y
* The description 
* The label 
* The unit 
* The standard unit 
* The display unit 
* The format of the displayed value 
* The minimum limit 
* The maximum limit 
* The minimum alarm level
* The maximum alarm level
* The minimum warning level
* The maximum warning level
* The delta t 
* The delta val
* The Attribute Name to write when the writable mode is "READ_WITH_WRITE"
* The Root Attribute Name when the Attribute is of ForwardedAttribute type
* The Display level
* The enumeration labels for the Enum Type
* The Event relative change
* The Event absolute change
* The Archive relative change
* The Archive absolute change
* The Event period
* The Archive period
* Any other configuration for future extensions
* Any other system configuration for future extensions

No reply is required

##### ATTRIBUTE HISTORY

Allows to query the attribute history up to the available depth.

An Attribute History request message SHALL contain:

* Attribute name
* Number of requested entries

An Attribute history reply message SHALL contain:

For version 3 a collection of tuples with the following elements:

* Attribute passed/failed state
* List of attribute values including:
  * Value
  * Quality
  * Time
  * Name
  * The dimension of the read part (x and y)
  * The dimension of the write part (x and y)
  * List of errors

The size of the returned collection is the minimum of the requested entries, the stored number of entries and configured depth.

For version 4 and above:

* Attribute name
* List of timestamps
* List of attribute qualities
* List of quality ranges defined as index and length
* List of x/y read dimensions
* List of read dimension ranges defined as index and length
* List of x/y write dimensions
* List of write dimension ranges defined as index and length
* List of list of errors
* List of list of ranges into the errors array defined as index and length
* data_type (one of Tango::AttributeDataType), since version 5
* data_format (one of Tango::SCALAR/SPECTRUM/IMAGE), since version 5

The size of the returned first-level lists is the minimum of the requested entries, the stored number of entries and configured depth.

##### PIPE GET CONFIG

The PIPE GET CONFIG request queries a Device and its reply informs a client of the Device about the Configurations of one or more Pipes in the Device.

A PIPE GET CONFIG request MUST be:

* A sequence of Pipe Names

A PIPE GET CONFIG reply MUST consist of a sequence of Pipe Information tuples.

A Pipe Information tuple MUST consist of:

 * The Pipe Name
 * The Pipe Description
 * The Pipe Label
 * The Pipe's Display Level as defined in [7/Pipe](../7/Pipe.md)
 * The Output Data Type as defined in [7/Pipe](../7/Pipe.md)
 * The Pipe Write Type as defined in [7/Pipe](../7/Pipe.md)


For reference, see also [7/Pipe](../7/Pipe.md).

##### PIPE SET CONFIG

The PIPE SET CONFIG request allows a Device's client to instruct a Device to set the Configurations for one or more of its Pipes.

A PIPE SET CONFIG request MUST be:
*  A sequence of Pipe Configuration tuples
*  A client ID

A Pipe Configuration tuple MUST consist of:

 * The Pipe Name
 * The Pipe Description
 * The Pipe Label
 * The Pipe's Display Level as defined in [7/Pipe](../7/Pipe.md)
 * The Pipe Write Type as defined in [7/Pipe](../7/Pipe.md)

For reference, see also [7/Pipe](../7/Pipe.md).

##### PIPE READ

A Pipe read request message SHALL contain:

* A name of pipe to read
* A client ID

A Pipe read reply message SHALL contain:
  
* A name
* A time
* A value with the Data Type of DevPipeBlob

For reference, see also [7/Pipe](../7/Pipe.md).

##### PIPE WRITE

A Pipe write request message SHALL contain:

* A name
* A time
* A value with Data Type of DevPipeBlob
* A client ID

In case of success the reply message MUST contain nothing, on error it SHALL contain error information.

For reference, see also [7/Pipe](../7/Pipe.md).

##### PIPE WRITE READ

A Pipe write read request message SHALL contain:

* A name
* A time
* A value with Data Type of DevPipeBlob
* A client ID

A Pipe write read reply message SHALL contain:
  
* A name
* A time
* A value with Data Type of DevPipeBlob

The data for the reply SHALL only be read after the write value has been written.

For reference, see also [7/Pipe](../7/Pipe.md).


#### Exception

Certain conditions may prevent a Request to be handled properly.

* If a Request cannot be handled properly, the Client SHALL be notified by receiving of a DevFailed or MultiDevFailed exception (see [9/DataTypes](../9/DataTypes.md#exceptions)).
* The DevFailed exception MAY be generated by either a Client (API) or a Device Server.
* Exceptions MAY be raised either by API code or by a user provided code.
* The Request Reply Protocol SHALL provide mechanism of sending a DevFailed or MultiDevFailded exception from a Device Server to a Client.
* If a Device Server throws a (Multi)DevFailed exception upon request processing, the exception SHALL be sent to the Client which sent the request.
* If a Client receives a (Multi)DevFailed exception it SHALL be available (rethrown) to user (code).

#### Timeout

* On the client side, the Request-Reply protocol SHALL implement timeout mechanism for every type of request. Timeout mechanism is returning DevFailed exception if a request cannot be served in specified time called Timeout.
* For each request the client MAY setup Timeout value or opt-out from using timeout mechanism which means that:
  - If the request is Synchronous, the client SHALL be blocked as long as it takes the request to be served. A timeout realted exception SHALL not be returned.
  - If the request is Asynchronous, the Client SHALL NOT be blocked. The Client SHALL be able to access the request result after it is served by a Device Server without any time limits. A timeout realted exception SHALL NOT be returned. 
* The default Timeout is 3 seconds.

#### Synchronous request

The client MAY send Synchronous Requests. 

When the client sends a Synchronous Request it SHOULD wait for a request to be processed.

If one client thread sends multiple Synchronous Requests sequentially, these SHALL be processed in the same order as these have been sent.

The client SHALL handle the Synchronous Request in the way that it blocks the calling client 
thread until the request is fully processed (a Device Server reply to the request and the result is 
available to the client) or timeout or other error appear.

Below is a diagram showing an example seqence: 

![Synchronous Call diagram](synchronous-call.png)

The client MAY allow multiple Synchronous Requests to be sent in parallel if these are sent by multiple client threads.

The Device Server MAY process multiple synchronous requests in parallel according to its [Serialization](#### Serialization).


#### Asynchronous request

The client MAY send Asynchronous Requests.

When the client sends an Asynchronous Request it MAY not wait for the request to be processed before sending another request.

The client thread which sends an Asynchronous Request MUST NOT be blocked for a time of the request being processed (except for a time needed for the request registration).

A result of the Asynchronous Request SHALL be available for the client.

The client SHALL be informed when the result of the Asynchronous Request is available.

The client MAY decide whether to process the result of Asynchronous Request:
* As soon as it is available (for example in the call-back pattern)
* or when it decides to process
* or skip processing of the result.

Below is a diagram showing an example seqence of Asynchronous Requests: 

![Asynchronous Call diagram](asynchronous-call.png)

A Device Server SHALL process any incoming requests in the order of their arrival and according to its [Serialization](#### Serialization).



#### Cache

The Cache is a circular buffer of recent responses for requests generated internally by the Device Server's Polling mechanism.

The Polling is periodically invoking a list of requests on Devices instantiated by a Device Server. The period between 2 same requests invocations is called a Polling Period. 

* The Cache and Polling mechanism SHALL be implemented by a Device Server.

* The Polling SHALL support the following requests:
  * read an Attribute (with the exception of a Forwarded Attribute which MUST NOT be polled)
  * execution of a Command with DevVoid `argin`.
  
* An attempt to poll a forwarded Attribute MUST raise an exception.

* The Polling Period MAY be configured separately for each Attribute or Command of each Device.

* A Device Class MAY implement default settings of the Polling for Attributes and Commands.

* A Client MAY change the Polling Period of any Attribute or Command of any Device.

* A Client MAY switch off/on a Polling for any Attribute or Command of any Device.

* If the Polling Period is set to 0, the Device Server may fill the Cache with arbitrary values and arbitrary associated timestamps at any moment. This is called Externally Triggered Polling.

* The Polling SHALL be configurable by a Client via the following Admin Device commands:
  * `AddObjPolling`:
    Add a new object (command or Attribute) to the list of object(s) to be polled. It is also with this command that the polling period is specified.
  * `RemObjPolling`:
    To remove one object (command or Attribute) from the polled object(s) list
  * `UpdObjPollingPeriod`:
    Change one object polling period
  * `StartPolling`:
    Starts polling for the whole process
  * `StopPolling`:
    Stops polling for the whole process
  * `PolledDevice`:
    Allow a client to know which devices are polled
  * `DevPollStatus`:
    Allow a client to know the polling status for a device precisely.

* The runtime behaviour of the Polling MAY be configured via the following properties of the Device Server Administration Device:
  * `polling_threads_pool_size` to specify number of polling threads.
  * `polling_threads_pool_conf` to assign instances of Devices (within the Device Server) to specific polling threads.
  * `polling_before_9` to set a polling algorithm version to use.
  * If `polling_before_9` property is not set or is set to `False` the Device Server's polling thread SHALL poll all attributes of a Device with the same polling period using a single device call (read_attributes) and allow the polling thread to be late but only if number of late objects decreases. This is a default algorithm.
  * If `polling_before_9` is present and set to `True`, the Device Server SHALL use the old (pre 9) version of polling mechanism (algorithm). In this case the Device Server's polling thread SHALL poll one Attribute at a time even if the polling period is the same (use of read_attribute) and do not allow the polling thread to be late.


* The Polling configuration defined by a Client SHALL be stored in the Database and applied upon a Device initialization.

* For each request from the Polling list, the Device Server SHALL have at least the most recent response generated by the request in a Cache.

* The depth of the Cache (the circular buffer size) SHALL be configurable using `poll_ring_depth` device property from the database.
  
* For any Request, a Client MAY decide whether it wants a response from:
  * The Cache
  * or execute the underlying request
  * or the Cache with fallback to underlying request if the Cache is empty or old. The Cache for certain Attribute or Command is considered old if the most recent value is older than Polling Period multiplied by `poll_old_factor` property from the Database.

* The source of Attribute Read request on a Forwarded Attribute MUST be applied to the Root Attribute Read request.

  The Device Server SHALL respond adequately.

* A Client MAY read Attribute's or Command's history from Cache (read last n values).
* The Server SHALL provide interface to read Attribute's and Command's history.
  
* If the client asks for the Cache without fallback and the Cache is old, the Device Server SHALL respond with DevFailedException with `<reason>` field set to `API_NotUpdatedAnyMore`.

* If the client asks for the Cache without fallback and the Cache is empty, the Device Server SHALL respond with DevFailedExcpetion with `<reason>` field set to `API_NoDataYet`.

* The Request-Reply protocol SHALL allow a Client to read history from the Cache. 

* If the Client asks for a history of not polled Attribute or Command, a Device Server SHALL return DevFailedException with `<reason>` field set to `API_AttrNotPolled` or `API_CmdNotPolled` respectively.

#### Serialization

The Serialization Mode defines how the Device Server handles multiple requests. 

The Serialization SHALL apply to all requests independently of a request origin (any network host, localhost, the same Device Server process). 

A Device Server MAY handle requests with the following Serialization Modes:
* Serialization by Device
* Serialization by Class
* Serialization by Process
* No Serialization

When a Device Server is set to Serialization by Device:
* It SHALL process requests to each Device in the order of requests arrival.
* For any Device in the Device Server, it MUST NOT start the processing of a new request until the processing of the previous request is not finished.
* It MAY process multiple requests in parallel, providing that there is maximum one processed request per a Device.

When a Device Server is set to Serialization by Class:
* It SHALL process requests to each set of Devices belonging to the same Class in the order of requests arrival.
* For each Device Class, it MUST NOT start the processing of a new request until the processing of the previous request is not finished.
* It MAY process multiple requests in parallel, providing that there is maximum one processed request per Device Class.

If Device Server is set to Serialization by Process:
* It SHALL process requests in the order of requests arrival.
* It MUST NOT start the processing of a new request until the processing of the previous request is not finished.

The Device Server SHOULD process requests as soon as possible. If an incoming request cannot be processed due to Device Server 
Serialization Mode it SHOULD be queued for later processing and served as soon as possible.

If Device Server is set to No Serialization it MAY process any requests in parallel. 
It is NOT RECOMMENDED to use No Serialization mode. 

The default serialization Mode is "Serialization by Device".

#### Blackbox
A blackbox system should record every REQUEST on the Device especially:

Attribute case
* Read Attribute request:
  Should register the attribute name, the id of the client (often the pid), the version of TANGO and the Source (DevSource).
* Write or WriteRead Attribute request:
  Should register the attribute name, attribute value (only for writing?) (AttributeValueList_4), the id of the client (often the pid) and the version of TANGO.
* Reading an Attribute configuration # (Op_Get_Attr_Config).
* Changing an Attribute Configuration # (Op_Set_Attr_Config).
* Read the Attribute History.

Pipe case
* Read Pipe request:
  Should register the pipe name, the id of the client (often the pid), the version of TANGO and the Source (DevSource).
* Write Pipe request:
  Should register the pipe name, attribute value, the id of the client (often the pid) and 0?, the version of TANGO_PIPE?.
* WriteRead Pipe request:
  Should register the pipe name, attribute value, the id of the client (often the pid) and 1?, the version of TANGO_PIPE?.
* Reading an Pipe configuration # (Op_Get_Pipe_Config_5).
* Changing an Pipe Configuration # (Op_Set_Pipe_Config_5).
* Read the Pipe History.

Command case
* Listing the list of Command Name [//]: #  (Op_Command_list).
* Execute a Command Name: Should register the command name, the id of the client (often the pid), the version of TANGO and the Source (DevSource).

* Any operation like reading the request of Command History [//]: #   Op_Command_inout_history_4.,

Operations case:
* Reading the BlackBox itself #  (Op_BlackBox).
* Reading the device (Attribute?) name # (Attr_Name).
* Request the adm_name attribute # (Attr_AdmName).
* Request thedescription attribute # (Attr_Description).
* Read the information of a device command  # (Op_Command).
* Read the information of a device # (Op_Info).
* Executing Ping # (Op_Ping).


NOTE All GROUPED REQUESTS should be logged once with the same information as the unique request

#### Device Locking
A Client has the possibility to avoid any modification of a Device from other Clients. This system is so-called Device Lock. The Device Server has here the role and the responsibility to apply this mechanism.

The following specifies the Device Lock system:  
* Only one Device Lock per Device SHALL be activated.
* A Device Lock SHALL be only owned by one Client.

When a Client activates a Device Lock, the Device Server MUST reject any of the following requests from any other Client:
* Command call except for the State Command, the Status Command and all commands listed in the Allowed Commands list (described further down).
* Write Attribute Request (including the atomic Write Read request).
* Write Pipe Request (including the atomic Write Read request).
* Setting Attribute Configuration.
* Setting Pipe Configuration.
* Setting Polling Configuration.
* Setting Logging Configuration.
In general it is RECOMMENDED to block any request resulting in a mutation of a Device.

An Allowed Command is not affected by the Device Lock, even if this command execution implies a modification of the Device state. A list of Allowed Commands are defined per Device Class with the `AllowedAccessCmd` Class Property of type DevVarStringArray.

+Other Clients have the possibility to check a Device Lock is currently activated via the Device Server Command '''DevLockStatus''' specified by:
+* Command Name: DevLockStatus
+* Argument Input of DevString type corresponding to the Device Name
+* Argument Output of DevVarLongStringArray type giving the Device Lock status with these mandatory values:
+** String values in order: the Client hostname
+** Long values in order: the activation state, the Client IP
+

+The request of a Device Lock is done through the DeviceServer command '''LockDevice''' with the Input Argument of DevVarLongStringArray with the following value:
+* As string value the Device Name to lock,
+* As long value the validity time in second. 
+The protocol of activation SHALL be the responsibility of the Device Server, following this sequence: ```
* Check no other Client owns an active and valid DeviceLock on the requested Device
* Activate the Device Lock 
* Increase the counter of Device Lock (See Below).
* Device Lock request SHALL be sent and validated by all Devices connected to the Forwarded Attributes of the targetted Device. 
In case of failure of the activation of a Device Lock the Device Server SHALL throw a related DevFailed exception with \<reason\> field set to "API_DeviceLocked".

A Client owning a Device Lock MAY:
* Revoke the Device Lock by the DeviceServer's UnLockDevice Command.
* Renew the Device Lock activation by the DeviceServer's ReLockDevices Command.
A Locking Tango Exception SHALL be thrown if the Device Lock is not active anymore.


A Device Lock is defined by:
* A Client Identification of the owner.
* A timestamp corresponding of the locking activation.
* A counter representing the number of valid activation from the same Client.
* The activation time in second defining the locking period from the last activation timestamp.

A Device Lock SHALL be deactivated in any of the following cases:
* The Client owning the Device Lock, calls the Device Server's UnLockDevice Command for the locked Device.
* Any Client calls the DeviceServer's UnLockDevice Command with a `force` flag set to true for the locked Device.
* The time of activation expired.
* The Connection to the Client is lost.
* The Device Server is reinitialised.

If the Device Lock has been disabled with the `force` flag set to true, the Server SHALL sent to the Client who originally owned the Device Lock a DevFailed exception with reason set to "API_DeviceUnlocked" upon next request from the Client.

A Client Identification SHOULD carry these information:
* Hostname where the Client process runs.
* PID of the Client process.
* IP of the Client connection.
It is recommended to include any other informations which may help the administrators of the system.  



#### Connection management

* The Request-Reply protocol SHALL manage client connection.

#### Connection establishement

A Connection MUST consist of exactly two parties:

- Client: The Connection Initiator
- Server: The Connection Target

##### Interoperable Object Reference

An implementation SHALL specify a Connection Target by an Interoperable Tango
Reference (ITR). An implementation MUST observe the following characteristics
of an ITR:

- ITRs SHALL serve as a mapping between Tango Controls object addresses and an implementation's object addresses.
- It SHALL be possible to address every publicly exported Tango Controls object in a Server by an ITR.
- It MAY be possible to address not publicly exported Tango Controls objects in
  a Server by an ITR.
- Every ITR MUST be globally unique.
- Every generated ITR MUST be reproducible.

