---
domain: rfc.tango-controls.org 
shortname: 7/Pipe
name: Pipe 
status: stable 
editor: Reynald Bourtembourg (reynald.bourtembourg@esrf.fr)
---

# 7/The Tango Pipe specification

This specification describes the Tango Pipe feature of a Device. 

## Preamble

Copyright (c) 2019 Tango Controls Community.

This Specification is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This Specification is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.

This Specification is a [free and open standard](https://web.archive.org/web/20161002091934/http://www.digistan.org/open-standard:definition) and is governed by the Digital Standards
Organization's [Consensus-Oriented Specification System](https://web.archive.org/web/20161002092144/http://www.digistan.org/spec:1/coss).

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC-2119](https://tools.ietf.org/html/rfc2119).

## Goals

A Tango Pipe is an Attribute [4/Attribute](../4/Attribute.md) with a flexible data structure, name and  description.
Unlike Commands or Attributes, a Pipe does not have a pre-defined data type. Tango Pipe data types may be a mixture of any of the basic Tango data types (or array of) and may change every time a pipe is written.

## Use Case

1. To define complex data structures to act as a single Attribute.
2. To pass data structures of arbitrary type and size.

## Specification

A Device MAY define zero, one or more Pipes.

Pipe MUST have static metadata:

 * name
 * display-level, an enumeration describing visibility level of the Pipe (one of EXPERT, OPERATOR)
 * writable

```abnf
name = attribute-name
display-level = "OPERATOR" / "EXPERT"
writable = "PIPE_READ" / "PIPE_WRITE"
```

Where `attribute-name` is defined in [4/Attribute](../4/Attribute.md).

Pipe with writable="PIPE_READ" MUST be read only.

Pipe MUST have modifable metadata:

 * description
 * label

```abnf
description = DevString
label = DevString
```

Where `DevString` is defined in [9/DataTypes](../9/DataTypes.md).

Pipe data structure MUST be of type `DevPipeBlob`, as specified in [9/DataTypes](../9/DataTypes.md).  See image below.

![](pipe_Tango_9.png)


> **V10 NOTE:** Attribute can be considered as a derivative of a Pipe.

### Pipe events

Pipe MUST NOT be polled.

Pipe MAY emit an event via Server API call.

### Server/Client APIs

Server API MUST provide a set of methods for a Server Client code to define a Pipe.

Server API MUST provide a way to fill the Pipe with data.

Client API MAY provide a way to obtain Pipe schema.

Client API MUST provide methods for a client to fetch Pipe data from a Device.

Client API MUST provide methods to traverse Pipe structure.

